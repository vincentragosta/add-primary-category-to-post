# Add Primary Category To Post #

A plugin to add a Primary Category meta box to the "Post" post type. Utility class included. Can use specific filters to extend functionality to custom post types.

## Recommended Installation ##

Although not limited too, this plugin was created to be used within a composer-based architecture.
 
In your composer.json file, under the Repositories section, add the following snippet of code:

```json
{
    "type": "composer",
    "url": "https://packages.vincentragosta.io"
}
```

Within your require section, please add the following dependency:

```json
"vincentragosta/add-primary-category-to-post": "dev-master"
```

Run `composer install` to install the plugin to your project, navigate to the WordPress plugins screen and activate your plugin.

## Utility Class ##

The plugin comes with a utility class called PostRepository. This class can be used and extended upon to query posts with the Primary Category associated meta.

```php
    public function findByPrimaryCategoryId(string $primary_category_id)
    {
        return $this->find([
            'meta_query' => [
                'key' => '_primary_category_id',
                'value' => $primary_category_id,
                'compare' => '='
            ]
        ]);
    }

    public function find(array $args)
    {
        $query = new \WP_Query(array_merge(
            apply_filters('add-primary-category-to-post/default-query-args', static::DEFAULT_QUERY_ARGS),
            $args
        ));
        return $query->found_posts ? $query->posts : [];
    }
``` 

## Customization ##

This plugin comes packaged with two filters:

`add-primary-category-to-post/post-types`: Use this filter to extend the Primary Category meta box to additional custom post types.

`add-primary-category-to-post/default-query-args`: Use this filter to manipulate the query arguments in the Utility class. 