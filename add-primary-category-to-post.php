<?php
/**
 * Plugin Name: Add Primary Category To Post
 * Description: A plugin to add a primary category to the Post Post Type. Can use various filters to extend functionality to custom post types.
 * Version: 1.0
 * License: GPL-2.0+
 **/

use AddPrimaryCategoryToPost\Controller\MetaBoxController;

defined('ABSPATH') || exit;
if (!defined('USE_COMPOSER_AUTOLOADER') || !USE_COMPOSER_AUTOLOADER) {
    require __DIR__ . '/vendor/autoload.php';
}

/**
 * Class AddPrimaryCategoryToPost
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class AddPrimaryCategoryToPost
{
    const POST_TYPES = ['post'];
    public function __construct()
    {
        add_action('init', function() {
            new MetaBoxController(apply_filters('add-primary-category-to-post/post-types', static::POST_TYPES));
        }, 1);
    }
}

new AddPrimaryCategoryToPost();

