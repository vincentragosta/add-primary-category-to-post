<?php

namespace AddPrimaryCategoryToPost\Controller;

/**
 * Class MetaBoxController
 * @package AddPrimaryCategoryToPost\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class MetaBoxController
{
    public function __construct(array $custom_post_types)
    {
        if (!empty($custom_post_types)) {
            foreach($custom_post_types as $post_type) {
                if (!is_string($post_type)) {
                    continue;
                }
                add_action('add_meta_boxes_' . $post_type, [$this, 'addPrimaryCategoryMetaBox']);
                add_action('save_post_' . $post_type, [$this, 'savePrimaryCategory'], 10, 2);
            }
        }
    }

    function addPrimaryCategoryMetaBox($post)
    {
        add_meta_box(
            'primary-category-meta-box',
            __('Select Primary Category', 'add-primary-category-to-post'),
            [$this, 'renderPrimaryCategoryMarkup'],
            $post->post_type,
            'side',
            'low'
        );
    }

    public function renderPrimaryCategoryMarkup($post)
    {
        wp_nonce_field(basename(__FILE__), 'primary_category_nonce');
        $primary_category_id = get_post_meta($post->ID, '_primary_category_id', true); ?>
        <div class='inside'>
        <h3><?php _e('Primary Category', 'add-primary-category-to-post'); ?></h3>
        <p>
            <select name="_primary_category_id">
                <option value="">-- Select Category --</option>
                <?php foreach (get_categories() as $Category): /* @var \WP_Term $Category */ ?>
                    <option value="<?= $Category->term_id; ?>" <?= $Category->term_id == $primary_category_id ? 'selected' : ''; ?>><?= $Category->name; ?></option>
                <?php endforeach; ?>
            </select>
        </p>
        </div><?php
    }

    public function savePrimaryCategory($post_id)
    {
        if (!isset($_POST['primary_category_nonce']) || !wp_verify_nonce($_POST['primary_category_nonce'], basename(__FILE__))) {
            return;
        }

        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        if (!current_user_can('edit_post', $post_id)) {
            return;
        }

        if (isset($_POST['_primary_category_id'])) {
            update_post_meta($post_id, '_primary_category_id', sanitize_text_field($_POST['_primary_category_id']));
        }
    }
}