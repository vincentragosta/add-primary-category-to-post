<?php

namespace AddPrimaryCategoryToPost\Repository;

/**
 * Class PostRepository
 * @package AddPrimaryCategoryToPost\Repository
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class PostRepository
{
    const DEFAULT_QUERY_ARGS = [
        'post_type' => 'post'
    ];

    public function findByPrimaryCategoryId(string $primary_category_id)
    {
        return $this->find([
            'meta_query' => [
                'key' => '_primary_category_id',
                'value' => $primary_category_id,
                'compare' => '='
            ]
        ]);
    }

    public function find(array $args)
    {
        $query = new \WP_Query(array_merge(
            apply_filters('add-primary-category-to-post/default-query-args', static::DEFAULT_QUERY_ARGS),
            $args
        ));
        return $query->found_posts ? $query->posts : [];
    }
}